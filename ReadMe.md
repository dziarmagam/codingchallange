## Application access

URL - [https://adverity-coding-challenge-2143234330.eu-central-1.elb.amazonaws.com](https://adverity-coding-challenge-2143234330.eu-central-1.elb.amazonaws.com)  
**Application database will be preloaded with CSV provided in a task.**   
**Please notice that https is protected by self-signed certificate which http client will not trust by default.**  
BasicAuth for protected endpoints - `adverity:UploadAndCleanup`

## REST API Documentation

###GET /api/v1/data/query - query endpoint
**body** - Query :  
*groupBy* - Array of Dimension, Group result by either "Campaign" or "Datasource" or both.  
*dateFilter* - Array of DateFilters, limit result by Date. Each filter contain 2 properties "fromInclusive" and "toInclusive" which are dates in ISO format.  
*campaignFilters* - Array of CampaignFilters. Each filter contain 1 string property "value" which would be name of Campaign  
*datasourceFilters* - Array of DatasourceFilters. Each filter contain 1 string property "value" which would be name of Datasource  
*aggregator* - Name of data aggregator. Available aggregators are - ClicksSum, ImpressionsSum, ClicksAveragePerDay, ImpressionsAveragePerDay, CTR

**Example request body**:
```json 
{
    "groupBy": [
        "Campaign",
        "Datasource"
    ],
    "dateFilters": [
        {
            "fromInclusive": "2019-11-12",
            "toInclusive": "2019-11-13"
        }
    ],
    "campaignFilters": [
        {
            "value": "SN_Online-Special_LG Fernseher"
        },
        {
            "value": "AT|SN|Snow Jacken|Brands"
        }
    ],
    "datasourceFilters": [
        {
            "value": "Twitter Ads"
        },
        {
            "value": "Facebook Ads"
        }
    ],
    "aggregator": "ImpressionsSum"
}
```  

**How filters work** - Each filter type is grouped and logical OR is performed between filters of the same type. Logical AND is performed between grouped filter of different type  
*Example* - Example request will produce following "where" SQL part:  
```
WHERE (Campagin = "SN_Online-Special_LG Fernseher" OR Campaign = "AT|SN|Snow Jacken|Brands")
AND (Datasource = "Twitter Ads" OR Datasource = "Facebook Ads")
AND (Daily BETWEEN "2019-11-12" AND "2019-11-13")
```

**Response** - list of QueryResult :  
*value* - result of aggregation  
*aggregator* - Name of aggregator passed in orginal request  
*campaign* - Campaign name if request was group by Campaign  
*datasource* - Datasource name if request was group by Datasource  
**Example response body**
```json
[
  {
    "value": 76.0,
    "aggregator": "ImpressionsSum",
    "campaign": "AT|SN|Snow Jacken|Brands",
    "datasource": "Facebook Ads"
  },
  {
    "value": 62.0,
    "aggregator": "ImpressionsSum",
    "campaign": "AT|SN|Snow Jacken|Brands",
    "datasource": "Twitter Ads"
  },
  {
    "value": 29.0,
    "aggregator": "ImpressionsSum",
    "campaign": "SN_Online-Special_LG Fernseher",
    "datasource": "Twitter Ads"
  }
]
```

###POST /admin/data/upload - upload new records via csv file, protected by BasicAuth
**body** - csv content

###POST /admin/data/cleanup - remove all records from database, protected by BasicAuth

###GET /health - application health status

###GET /version - application version

## Deployment

Application is deployed to AWS ECS.  
There are 2 application instances running which are connecting to postgres RDS.  
Access to the API is via Loadbalancer which provide https connection (with self-sign certificate).
