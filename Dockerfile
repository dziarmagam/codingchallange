FROM adoptopenjdk/openjdk11
COPY build/distributions/AdverityCodingChallenge-1.0-SNAPSHOT.tar app.tar
RUN tar -xf app.tar
RUN cd AdverityCodingChallenge*-SNAPSHOT
ENTRYPOINT AdverityCodingChallenge*-SNAPSHOT/bin/AdverityCodingChallenge