package adverity.challenge.http

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.KotlinModule

val jsonMapper: ObjectMapper = ObjectMapper()
    .registerModule(KotlinModule())
    .registerModule(JavaTimeModule())