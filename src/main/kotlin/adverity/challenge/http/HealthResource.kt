package adverity.challenge.http

import com.google.common.annotations.VisibleForTesting
import com.google.common.util.concurrent.ThreadFactoryBuilder
import org.http4k.core.Method.GET
import org.http4k.core.Request
import org.http4k.core.Response
import org.http4k.core.Status
import org.http4k.routing.bind
import org.http4k.routing.routes
import org.slf4j.LoggerFactory
import java.io.Closeable
import java.util.concurrent.Executors
import java.util.concurrent.ScheduledExecutorService
import java.util.concurrent.TimeUnit
import java.util.concurrent.atomic.AtomicReference

class HealthResource(
    private val healthReporters: Collection<HealthReporter>,
    private val executor: ScheduledExecutorService = Executors.newScheduledThreadPool(
        1, ThreadFactoryBuilder()
            .setDaemon(true)
            .setNameFormat("HealthCheckThread")
            .build()
    )
) : Closeable {

    private val logger = LoggerFactory.getLogger(this::class.java)
    private val currentHealthReport = AtomicReference(HealthReport(false, "Service health check", "Service is starting", emptyList()))

    init {
        logger.info("Starting HealthCheck loop")
        executor.scheduleAtFixedRate(this::updateHealth, 0L, 10L, TimeUnit.SECONDS)
        logger.info("HealthCheck loop is running")
    }

    val routes = routes(
        "/health" bind GET to this::getHealth
    )

    @VisibleForTesting
    fun updateHealth() {
        logger.debug("Updating health check status")
        val healthReports = healthReporters.map { it.getHealthReport() }
        val isServiceHealthy = healthReports.all { it.isHealthy }
        currentHealthReport.set(HealthReport(isServiceHealthy, "Service health check", information = "Service is running", healthReports))
        logger.debug("Health check status updated")
    }

    private fun getHealth(request: Request): Response {
        val healthReport = currentHealthReport.get()
        val returnStatus = if (healthReport.isHealthy) Status.OK else Status.INTERNAL_SERVER_ERROR
        return Response(returnStatus).body(jsonMapper.writeValueAsString(healthReport))

    }

    override fun close() {
        this.executor.shutdownNow()
    }
}

