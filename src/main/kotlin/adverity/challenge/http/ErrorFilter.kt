package adverity.challenge.http

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonInclude.*
import org.http4k.core.*
import org.slf4j.LoggerFactory

class ErrorFilter : Filter {

    private val logger = LoggerFactory.getLogger(this::class.java)

    override fun invoke(p1: HttpHandler): HttpHandler {
        return { request: Request ->
            try {
                p1.invoke(request)
            } catch (e: Exception) {
                logger.error("Error while handling request to ${request.uri}", e)
                createResponseFromException(e)
            }
        }
    }


}

@JsonInclude(Include.NON_NULL)
data class RequestError(val info: String?, val exceptionMessage: String?)

fun createResponseFromException(e: Exception, status: Status = Status.INTERNAL_SERVER_ERROR, info: String? = null): Response {
    return Response(status)
        .body(jsonMapper.writeValueAsString(RequestError(info, e.message)))
        .header("content-type","application/json")
}
