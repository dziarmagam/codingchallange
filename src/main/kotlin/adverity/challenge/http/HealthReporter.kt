package adverity.challenge.http

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonInclude.*

interface HealthReporter {
    fun getHealthReport(): HealthReport
}
@JsonInclude(Include.NON_NULL)
data class HealthReport(val isHealthy: Boolean,
                        val name: String,
                        val information: String?,
                        val dependencies: List<HealthReport>?)