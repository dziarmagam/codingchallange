package adverity.challenge.http

import com.fasterxml.jackson.module.kotlin.readValue
import org.http4k.core.Method
import org.http4k.core.Request
import org.http4k.core.Response
import org.http4k.core.Status
import org.http4k.routing.bind
import org.http4k.routing.routes
import org.slf4j.LoggerFactory

class VersionResource(versionFilePath: String) {

    private val currentVersion : ServiceVersion = jsonMapper.readValue(this::class.java.getResourceAsStream(versionFilePath))


    val routes = routes(
        "/version" bind Method.GET to this::getCurrentVersion
    )

    private fun getCurrentVersion(request: Request): Response {
        return Response(Status.OK).body(jsonMapper.writeValueAsString(currentVersion))
    }


}

data class ServiceVersion(val version: String, val gitHash: String)