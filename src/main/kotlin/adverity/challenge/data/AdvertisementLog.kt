package adverity.challenge.data

import java.time.LocalDate

data class AdvertisementLog(
    val datasource: String,
    val campaign: String,
    val daily: LocalDate,
    val clicks: Long,
    val impressions: Long
)
