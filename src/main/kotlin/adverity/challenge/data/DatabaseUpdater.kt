package adverity.challenge.data

import liquibase.Contexts
import liquibase.LabelExpression
import liquibase.Liquibase
import liquibase.database.DatabaseFactory
import liquibase.database.jvm.JdbcConnection
import liquibase.resource.ClassLoaderResourceAccessor
import org.jooq.SQLDialect
import org.slf4j.LoggerFactory
import javax.sql.DataSource

class DatabaseUpdater(
    private val dataSource: DataSource,
    private val sqlDialect: SQLDialect
) {

    private val changelogLocation = "/liquidbase/changelog.xml"
    private val logger = LoggerFactory.getLogger(this::class.java)

    fun update() {
        if (sqlDialect == SQLDialect.POSTGRES || sqlDialect == SQLDialect.H2) {
            logger.info("Updating database schema")
            dataSource.connection.use {
                val database = DatabaseFactory.getInstance().findCorrectDatabaseImplementation(JdbcConnection(it))
                Liquibase(changelogLocation, ClassLoaderResourceAccessor(), database)
                    .update(Contexts(), LabelExpression())
            }
        } else {
            logger.warn("Skipping schema database. Not supported database type")
        }
    }

}