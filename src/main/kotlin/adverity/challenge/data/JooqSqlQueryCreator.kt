package adverity.challenge.data

import adverity.challenge.api.*
import adverity.challenge.api.Query
import org.jooq.*
import org.jooq.impl.DSL
import org.slf4j.LoggerFactory
import java.time.LocalDate

class JooqSqlQueryCreator(private val dlsContext: DSLContext) {

    private val logger = LoggerFactory.getLogger(this::class.java)

    companion object {
        private val CLICKS_COLUMN = DSL.field("clicks", Long::class.java)
        private val DATASOURCE_COLUMN = DSL.field("datasource", String::class.java)
        private val IMPRESSIONS_COLUMN = DSL.field("impressions", Long::class.java)
        private val CAMPAIGN_COLUMN = DSL.field("campaign", String::class.java)
        private val DAILY_COLUMN = DSL.field("daily", LocalDate::class.java)
        private const val METRIC_RESULT_NAME = "Metric"
        private val groupByMap = mapOf(
            DimensionType.Campaign to CAMPAIGN_COLUMN,
            DimensionType.Datasource to DATASOURCE_COLUMN
        )
        private const val ADVERTISEMENT_LOG_DATA_TABLE = "advertisement_log"
    }

    fun create(query: Query): SelectHavingStep<Record> {
        val selectValue = createSelectPart(query)

        val whereValue = createWhereQueryPart(query)
        val groupByPart = createGroupByPart(query)

        val selectQuery = dlsContext.select(selectValue)
            .from(ADVERTISEMENT_LOG_DATA_TABLE)
            .where(whereValue).apply { if (query.aggregator == Metric.CTR) and(CLICKS_COLUMN.notEqual(0)) }
            .apply { if (groupByPart.isNotEmpty()) groupBy(groupByPart) }
        logger.debug("For query $query, jooq produced following sql: ${selectQuery.sql}")
        return selectQuery
    }

    private fun createSelectPart(query: Query): List<Field<out Any>> {
        val metricSelect = when (query.aggregator) {
            Metric.CTR -> DSL.sum(IMPRESSIONS_COLUMN).div(DSL.sum(CLICKS_COLUMN))
            Metric.ClicksAveragePerDay -> DSL.avg(CLICKS_COLUMN)
            Metric.ClicksSum -> DSL.sum(CLICKS_COLUMN)
            Metric.ImpressionsAveragePerDay -> DSL.avg(IMPRESSIONS_COLUMN)
            Metric.ImpressionsSum -> DSL.sum(IMPRESSIONS_COLUMN)
        }.`as`(METRIC_RESULT_NAME)

        val groupBySelect = query.groupBy.map { DSL.field(it.name, String::class.java) }
        return groupBySelect.plus(metricSelect)
    }

    private fun createGroupByPart(query: Query): List<Field<String>?> {
        return query.groupBy.map { groupByMap[it] }
    }

    private fun createWhereQueryPart(query: Query): Condition? {
        val dateFilters = query.filters.filterIsInstance<DateFilter>().map {
            DAILY_COLUMN.between(it.fromInclusive, it.toInclusive)
        }.ifEmpty { null }?.reduce { con1, con2 -> con1.or(con2) }
        val datasourceFilters = query.filters.filterIsInstance<DatasourceFilter>().map {
            DATASOURCE_COLUMN.equalIgnoreCase(it.value)
        }.ifEmpty { null }?.reduce { con1, con2 -> con1.or(con2) }
        val campaignFilters = query.filters.filterIsInstance<CampaignFilter>().map {
            CAMPAIGN_COLUMN.equalIgnoreCase(it.value)
        }.ifEmpty { null }?.reduce { con1, con2 -> con1.or(con2) }

        return listOfNotNull(datasourceFilters, dateFilters, campaignFilters)
            .ifEmpty { null }
            ?.reduce { con1, con2 -> con1.and(con2) }
    }
}