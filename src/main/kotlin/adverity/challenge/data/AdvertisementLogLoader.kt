package adverity.challenge.data

import org.apache.commons.csv.CSVFormat
import org.apache.commons.csv.CSVRecord
import org.slf4j.LoggerFactory
import java.io.InputStream
import java.time.LocalDate
import java.time.format.DateTimeFormatter

interface AdvertisementLogLoader {
    fun load(csvContent: InputStream)
}

class CsvAdvertisementLogLoader(private val advertisementLogUpdater: AdvertisementLogUpdater) : AdvertisementLogLoader {

    private val logger = LoggerFactory.getLogger(this::class.java)

    companion object {
        private val CVS_DATE_PATTERN = DateTimeFormatter.ofPattern("MM/dd/yy")
        private const val CLICKS_HEADER = "Clicks"
        private const val IMPRESSIONS_HEADER = "Impressions"
        private const val DATASOURCE_HEADER = "Datasource"
        private const val CAMPAIGN_HEADER = "Campaign"
        private const val DAILY_HEADER = "Daily"
        private val HEADERS = listOf(DATASOURCE_HEADER, CAMPAIGN_HEADER, DAILY_HEADER, CLICKS_HEADER, IMPRESSIONS_HEADER)
    }

    /**
     * We are loading every record into memory first which could result in out of memory exception
     * in case of big csv file. It should be fine tho for example purposes.
     */
    override fun load(csvContent: InputStream) {
        val parsedCsv = CSVFormat.DEFAULT.builder()
            .setHeader(*HEADERS.toTypedArray())
            .setAllowMissingColumnNames(false)
            .setTrim(true)
            .build()
            .parse(csvContent.bufferedReader())

        val records = parsedCsv.records
        logger.info("Uploading ${records.size} records")
        validateHeaders(records.first())
        advertisementLogUpdater.addRecords(records.drop(1).map(this::mapCsvRecordToObject))
    }

    private fun validateHeaders(headerRecord: CSVRecord?) {
        if (headerRecord == null) throw InvalidCsvHeaders(emptyList())
        if (headerRecord.size() != HEADERS.size) throw InvalidCsvHeaders(headerRecord.toList())
        if ((headerRecord[CLICKS_HEADER] == CLICKS_HEADER)
                .and(headerRecord[CAMPAIGN_HEADER] == CAMPAIGN_HEADER)
                .and(headerRecord[DAILY_HEADER] == DAILY_HEADER)
                .and(headerRecord[DATASOURCE_HEADER] == DATASOURCE_HEADER)
                .and(headerRecord[IMPRESSIONS_HEADER] == IMPRESSIONS_HEADER)
                .not()
        ) {
            throw InvalidCsvHeaders(headerRecord.toList())
        }
    }

    private fun mapCsvRecordToObject(record: CSVRecord): AdvertisementLog {
        return try {
            AdvertisementLog(
                datasource = record.get(DATASOURCE_HEADER),
                campaign = record.get(CAMPAIGN_HEADER),
                daily = LocalDate.parse(record.get(DAILY_HEADER), CVS_DATE_PATTERN),
                clicks = record.get(CLICKS_HEADER).toLong(),
                impressions = record.get(IMPRESSIONS_HEADER).toLong()
            )
        } catch (e: Exception) {
            throw  CsvParsingFailed(record.recordNumber, e.message)
        }
    }


    class InvalidCsvHeaders(val givenHeaders: List<String>) : RuntimeException("Csv headers are incorrect, expected $HEADERS but was $givenHeaders")
    class CsvParsingFailed(val recordNumber: Long, val originalMessage: String?) :
        RuntimeException("Failed parsing record $recordNumber, message: $originalMessage")
}