package adverity.challenge.data

import adverity.challenge.api.*
import adverity.challenge.http.HealthReport
import adverity.challenge.http.HealthReporter
import org.jooq.Record
import org.slf4j.LoggerFactory
import java.lang.Exception
import java.math.BigDecimal
import java.sql.Date
import java.sql.PreparedStatement
import javax.sql.DataSource

interface AdvertisementLogDao {

    fun query(query: Query): List<QueryResult>
}

interface AdvertisementLogUpdater {
    fun addRecords(records: List<AdvertisementLog>)
    fun removeAllRecords()
}

class PostgresAdvertisementLogDao(
    private val dataSource: DataSource,
    private val queryCreator: JooqSqlQueryCreator
) : AdvertisementLogDao, AdvertisementLogUpdater, HealthReporter {

    companion object {
        private const val INSERT_QUERY = "INSERT INTO advertisement_log(datasource, campaign, daily, clicks, impressions) VALUES (?,?,?,?,?)"
        private const val CLEANUP_QUERY = "DELETE FROM advertisement_log"
    }

    private val logger = LoggerFactory.getLogger(this::class.java)

    override fun addRecords(records: List<AdvertisementLog>) {
        dataSource.connection.use {
            it.autoCommit = false
            val ps =
                it.prepareStatement(INSERT_QUERY)
            records.forEach { record ->
                setInsertParameters(record, ps)
                ps.addBatch()
            }
            ps.executeLargeBatch()
            it.commit()
        }
    }

    override fun removeAllRecords() {
        dataSource.connection.use {
            it.prepareStatement(CLEANUP_QUERY).execute()
        }
    }

    override fun query(query: Query): List<QueryResult> {
        val preparedQuery = queryCreator.create(query)
        val fetchResult = preparedQuery.fetch()

        return if (fetchResult.isNotEmpty) {
            return fetchResult.map {
                QueryResult(
                    (it["Metric"] as BigDecimal).toDouble(),
                    query.aggregator,
                    getOrNull(it, "Campaign"),
                    getOrNull(it, "Datasource")
                )
            }
        } else {
            emptyList()
        }
    }

    private fun <T> getOrNull(it: Record, fieldName: String): T? {
        return try {
            it[fieldName] as T
        } catch (e: IllegalArgumentException) {
            logger.debug("No value for field $fieldName", e)
            null
        }
    }


    private fun setInsertParameters(record: AdvertisementLog, ps: PreparedStatement) {
        ps.setString(1, record.datasource)
        ps.setString(2, record.campaign)
        ps.setDate(3, Date.valueOf(record.daily))
        ps.setLong(4, record.clicks)
        ps.setLong(5, record.impressions)
    }

    override fun getHealthReport(): HealthReport {
        val name = "Database connection"
        return try {
            dataSource.connection.use {
                if (it.isClosed) {
                    HealthReport(false, name, "Database connection is closed", null)
                } else {
                    HealthReport(true, name, null, null)
                }
            }
        } catch (e: Exception) {
            logger.error("Error while getting health check for database connection", e)
            HealthReport(false, name, "Error while connecting to database", null)
        }
    }


}