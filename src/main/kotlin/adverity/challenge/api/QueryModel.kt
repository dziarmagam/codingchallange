package adverity.challenge.api

import com.fasterxml.jackson.annotation.JsonInclude
import java.time.LocalDate

/**
 * Those are domain model object.
 * In this simple case domain model and presentation model (return by REST API) is the same.
 * In cases where above is not true we would create separate object for presentation and domain model and we would do mapping between them.
 */

@JsonInclude(JsonInclude.Include.NON_NULL)
data class QueryResult(
    val value: Double,
    val aggregator: Metric,
    val campaign: String? = null,
    val datasource: String? = null
)

data class Query(
    val filters: Set<QueryFilter> = emptySet(),
    val groupBy: Set<DimensionType> = emptySet(),
    val aggregator: Metric,
)

sealed class QueryFilter
data class DateFilter(val fromInclusive: LocalDate, val toInclusive: LocalDate) : QueryFilter()
data class CampaignFilter(val value: String) : QueryFilter()
data class DatasourceFilter(val value: String) : QueryFilter()

enum class DimensionType {
    Campaign,
    Datasource
}

enum class Metric {
    ClicksSum,
    ImpressionsSum,
    ClicksAveragePerDay,
    ImpressionsAveragePerDay,
    CTR
}
