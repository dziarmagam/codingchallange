package adverity.challenge.api

import adverity.challenge.http.createResponseFromException
import adverity.challenge.http.jsonMapper
import org.http4k.core.Method.GET
import org.http4k.core.Request
import org.http4k.core.Response
import org.http4k.core.Status
import org.http4k.routing.bind
import org.http4k.routing.routes
import org.slf4j.LoggerFactory

class QueryResource(private val queryService: QueryService) {

    private val logger = LoggerFactory.getLogger(QueryResource::class.java)

    val routes = routes(
        "/data/query" bind GET to this::queryData
    )

    private fun queryData(request: Request): Response {
        val query = try {
            jsonMapper.readValue(request.bodyString(), QueryDto::class.java).mapToDomainObject()
        } catch (e: Exception) {
            logger.warn("Error while parsing query data request body", e)
            return createResponseFromException(e, Status.BAD_REQUEST, "Error while parsing query data request body")
        }

        val result = queryService.queryData(query)
        return Response(Status.OK).body(jsonMapper.writeValueAsString(result))
            .header("Content-type", "application/json")
    }
}

data class QueryDto(
    val groupBy: Set<DimensionType>,
    val aggregator: Metric,
    val dateFilters: Set<DateFilter> = emptySet(),
    val campaignFilters: Set<CampaignFilter> = emptySet(),
    val datasourceFilters: Set<DatasourceFilter> = emptySet()
) {

    companion object {
        const val MAX_FILTERS = 10
    }

    fun mapToDomainObject(): Query {
        val filters = dateFilters.plus(campaignFilters).plus(datasourceFilters)
        if (filters.size > MAX_FILTERS) {
            throw TooManyFiltersException(MAX_FILTERS, filters.size)
        }
        return Query(
            filters = filters,
            aggregator = aggregator,
            groupBy = groupBy
        )

    }
}

class TooManyFiltersException(val maxFilters: Int, val currentFilterNumber: Int) :
    RuntimeException("Too many filters passed in the query, maximum number is $maxFilters but was $currentFilterNumber")

