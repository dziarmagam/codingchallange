package adverity.challenge.api

import adverity.challenge.data.AdvertisementLogDao
import org.slf4j.LoggerFactory

class QueryService(private val advertisementLogDao: AdvertisementLogDao) {

    private val logger = LoggerFactory.getLogger(this::class.java)

    fun queryData(query: Query): List<QueryResult> {
        logger.info("Running query $query")
        val queryStartTime = System.currentTimeMillis()
        val queryResult = advertisementLogDao.query(query)
        logger.info("It took ${System.currentTimeMillis() - queryStartTime}ms to execute query:$query")
        return queryResult
    }

}
