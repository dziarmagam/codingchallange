package adverity.challenge.api

import adverity.challenge.data.AdvertisementLogUpdater
import org.http4k.core.Method
import org.http4k.core.Request
import org.http4k.core.Response
import org.http4k.core.Status
import org.http4k.routing.bind
import org.http4k.routing.routes
import org.slf4j.LoggerFactory

class CleanupResource(private val advertisementLogUpdater: AdvertisementLogUpdater) {

    private val logger = LoggerFactory.getLogger(this::class.java)
    val routes = routes(
        "/data/clean" bind Method.POST to this::cleanData
    )

    private fun cleanData(request: Request): Response {
        logger.info("Cleaning up all advertisement log data")
        advertisementLogUpdater.removeAllRecords()
        return Response(Status.OK)
    }
}