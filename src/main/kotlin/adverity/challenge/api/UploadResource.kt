package adverity.challenge.api

import adverity.challenge.data.AdvertisementLogLoader
import org.http4k.core.Method
import org.http4k.core.Request
import org.http4k.core.Response
import org.http4k.core.Status
import org.http4k.routing.bind
import org.http4k.routing.routes
import org.slf4j.LoggerFactory

class UploadResource(private val dataLoader: AdvertisementLogLoader) {

    private val logger = LoggerFactory.getLogger(this::class.java)

    val routes = routes(
        "/data/upload" bind Method.POST to this::uploadData
    )

    private fun uploadData(request: Request): Response {
        logger.info("Uploading data")
        dataLoader.load(request.body.stream)
        logger.info("Data upload finished")
        return Response(Status.OK)
    }
}