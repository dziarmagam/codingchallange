package adverity.challenge

import adverity.challenge.api.CleanupResource
import adverity.challenge.http.HealthResource
import adverity.challenge.api.QueryResource
import adverity.challenge.api.QueryService
import adverity.challenge.api.UploadResource
import adverity.challenge.config.*
import adverity.challenge.data.CsvAdvertisementLogLoader
import adverity.challenge.data.DatabaseUpdater
import adverity.challenge.data.JooqSqlQueryCreator
import adverity.challenge.data.PostgresAdvertisementLogDao
import adverity.challenge.http.ErrorFilter
import adverity.challenge.http.VersionResource
import org.apache.commons.dbcp2.BasicDataSource
import org.http4k.core.then
import org.http4k.filter.ServerFilters
import org.http4k.routing.bind
import org.http4k.routing.routes
import org.http4k.server.Netty
import org.http4k.server.asServer
import org.jooq.SQLDialect
import org.jooq.impl.DSL
import org.slf4j.LoggerFactory
import javax.sql.DataSource


class Application(config: Config) {
    private val logger = LoggerFactory.getLogger(Application::class.java)
    private val port = config.port

    private val dataSource = createDatasource(config.databaseConfig)
    private val jooqSqlQueryCreator = JooqSqlQueryCreator(DSL.using(dataSource, SQLDialect.POSTGRES))
    private val advertisementLogDao = PostgresAdvertisementLogDao(dataSource, jooqSqlQueryCreator)
    private val databaseUpdater = DatabaseUpdater(dataSource, config.databaseConfig.sqlDialect)
    private val queryService = QueryService(advertisementLogDao)
    private val queryResource = QueryResource(queryService)
    private val healthResource = HealthResource(listOf(advertisementLogDao))
    private val dataLoader = CsvAdvertisementLogLoader(advertisementLogDao)
    private val uploadResource = UploadResource(dataLoader)
    private val cleanupResource = CleanupResource(advertisementLogDao)
    private val versionResource = VersionResource("/version.json")

    private val mainRoute = routes(
        "/api/v1" bind routes(queryResource.routes),
        "/admin" bind ServerFilters.BasicAuth("adverity", "adverity", "UploadAndCleanup").then(
            routes(
                uploadResource.routes,
                cleanupResource.routes
            )
        ),
        healthResource.routes,
        versionResource.routes
    ).withFilter(ErrorFilter())

    fun start() {
        logger.info("Starting application")
        Runtime.getRuntime().addShutdownHook(Thread {
            logger.info("Shouting down application")
        })
        databaseUpdater.update()
        mainRoute.asServer(Netty(port)).start()
        logger.info("Application is running")
    }

    private fun createDatasource(databaseConfig: DatabaseConfig): DataSource {
        return BasicDataSource().apply {
            driverClassName = databaseConfig.driverClassName
            password = databaseConfig.password
            username = databaseConfig.username
            url = databaseConfig.url
        }

    }
}

fun main() {
    val configLoader = when (System.getenv()["ENV"]) {
        "DOCKER" -> DockerConfigLoader()
        else -> LocalConfigLoader()
    }

    Application(configLoader.load()).start()
}