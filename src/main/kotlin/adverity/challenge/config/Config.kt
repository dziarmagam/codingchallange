package adverity.challenge.config

import org.jooq.SQLDialect

data class Config(
    val port: Int,
    val databaseConfig: DatabaseConfig
)

data class DatabaseConfig(
    val driverClassName: String,
    val password: String,
    val username: String,
    val url: String,
    val sqlDialect: SQLDialect
)

