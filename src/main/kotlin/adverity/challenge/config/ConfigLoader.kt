package adverity.challenge.config

import org.jooq.SQLDialect

interface ConfigLoader {
    fun load(): Config
}

class LocalConfigLoader : ConfigLoader {
    override fun load(): Config {
        return Config(
            port = 8000,
            databaseConfig = DatabaseConfig(
                driverClassName = "org.postgresql.Driver",
                password = "mysecretpassword",
                username = "postgres",
                url = "jdbc:postgresql://localhost:5432/postgres",
                sqlDialect = SQLDialect.POSTGRES
            )
        )
    }
}