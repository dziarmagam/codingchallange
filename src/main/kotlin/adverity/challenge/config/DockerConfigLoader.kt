package adverity.challenge.config

import org.jooq.SQLDialect

class DockerConfigLoader : ConfigLoader {

    companion object {
        const val HTTP_PORT = "http_port"
        const val DATABASE_URL = "database_url"
        const val DATABASE_USER = "database_user"
        const val DATABASE_PASSWORD = "database_password"
        const val DATABASE_DIALECT = "database_dialect"
        const val DATABASE_DRIVER = "database_driver"

    }

    override fun load(): Config {
        val httpPort = getPropertyOrThrow(HTTP_PORT).toInt()
        val databaseUrl = getPropertyOrThrow(DATABASE_URL)
        val databaseUser = getPropertyOrThrow(DATABASE_USER)
        val databasePassword = getPropertyOrThrow(DATABASE_PASSWORD)
        val databaseDialect = getPropertyOrThrow(DATABASE_DIALECT).let { SQLDialect.valueOf(it) }
        val databaseDriver = getPropertyOrThrow(DATABASE_DRIVER)

        return Config(httpPort, DatabaseConfig(databaseDriver, databasePassword, databaseUser, databaseUrl, databaseDialect))

    }

    private fun getPropertyOrThrow(propertyName: String) = System.getenv().getOrElse(propertyName) { throw  MissingEnvironmentProperty(propertyName) }

}

class MissingEnvironmentProperty(val propertyName: String) : RuntimeException("Environment property $propertyName is missing")