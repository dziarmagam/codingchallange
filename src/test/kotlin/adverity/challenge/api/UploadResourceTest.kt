package adverity.challenge.api

import adverity.challenge.data.AdvertisementLogLoader
import org.assertj.core.api.Assertions
import org.http4k.core.Method
import org.http4k.core.Request
import org.http4k.core.Status
import org.junit.jupiter.api.Test
import java.io.InputStream

internal class UploadResourceTest {


    @Test
    fun `should call dataLoader when requesting data upload`(){
        //given
        val request = Request(Method.POST, "/data/upload").body("test")
        //when
        val response = UploadResource(mockDataLoader).routes.invoke(request)
        //then
        Assertions.assertThat(response.status).isEqualTo(Status.OK)
        val requestContent = mockDataLoader.passedInputStream?.readAllBytes()?.let { String(it) }
        Assertions.assertThat(requestContent).isEqualTo("test")
    }

    object mockDataLoader : AdvertisementLogLoader{
        var passedInputStream: InputStream? = null
        override fun load(csvContent: InputStream) {
            this.passedInputStream = csvContent
        }

    }
}