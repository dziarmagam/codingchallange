package adverity.challenge.api

import adverity.challenge.data.AdvertisementLogDao
import org.junit.jupiter.api.Test
import org.mockito.Mockito

internal class QueryServiceTest {


    @Test
    fun `should invoke dataWarehouseDao query method when querying for data`() {
        //given
        val advertisementLogDao = Mockito.mock(AdvertisementLogDao::class.java)
        val query = Query(aggregator = Metric.CTR)
        //when
        QueryService(advertisementLogDao).queryData(query)
        //then
        Mockito.verify(advertisementLogDao).query(query)
    }

}