package adverity.challenge.api

import adverity.challenge.http.jsonMapper
import org.assertj.core.api.Assertions
import org.http4k.core.Method
import org.http4k.core.Request
import org.http4k.core.Status
import org.junit.jupiter.api.Test
import org.mockito.Mockito

internal class QueryResourceTest {

    private val service: QueryService = Mockito.mock(QueryService::class.java)
    private val queryResource = QueryResource(service)


    @Test
    fun `should return 400 if request body is not valid Query object`(){
        //given
        val query = """{"invalid":"json"}"""
        val request = Request(Method.GET,"/data/query")
            .body(query)
        //when
        val response = queryResource.routes(request)
        //then
        Assertions.assertThat(response.status).isEqualTo(Status.BAD_REQUEST)
    }

    @Test
    fun `should return 400 if request has to many filters`(){
        //given
        val query = QueryDto(groupBy = setOf(), aggregator = Metric.ClicksSum, campaignFilters = setOf(
            CampaignFilter("1"),
            CampaignFilter("2"),
            CampaignFilter("3"),
            CampaignFilter("4"),
            CampaignFilter("5"),
            CampaignFilter("6"),
            CampaignFilter("7"),
            CampaignFilter("8"),
            CampaignFilter("9"),
            CampaignFilter("10"),
            CampaignFilter("11")
        ))
        val request = Request(Method.GET,"/data/query")
            .body(jsonMapper.writeValueAsString(query))
        //when
        val response = queryResource.routes(request)
        //then
        Assertions.assertThat(response.status).isEqualTo(Status.BAD_REQUEST)
        Assertions.assertThat(response.bodyString()).containsIgnoringCase("too many filters")
    }

    @Test
    fun `given valid query should return 200 when doing query request`(){
        //given
        val query = QueryDto(groupBy = setOf(), aggregator = Metric.ClicksSum)
        val request = Request(Method.GET,"/data/query")
            .body(jsonMapper.writeValueAsString(query))
        val queryResult = QueryResult(10.0, aggregator = Metric.CTR)
        Mockito.`when`(service.queryData(query.mapToDomainObject())).thenReturn(listOf(queryResult))
        //when
        val response = queryResource.routes(request)
        //then
        Assertions.assertThat(response.status).isEqualTo(Status.OK)
        Assertions.assertThat(response.header("Content-type")).isEqualTo("application/json")
        Assertions.assertThat(response.bodyString()).isEqualTo(jsonMapper.writeValueAsString(listOf(queryResult)))

    }
}