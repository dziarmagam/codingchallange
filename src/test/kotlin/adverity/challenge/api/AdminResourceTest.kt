package adverity.challenge.api

import adverity.challenge.data.AdvertisementLogUpdater
import org.assertj.core.api.Assertions
import org.http4k.core.Method
import org.http4k.core.Request
import org.http4k.core.Status
import org.junit.jupiter.api.Test
import org.mockito.Mockito

internal class AdminResourceTest {


    @Test
    fun `should cleanup data records and return 200`(){
        //given
        val advertisementLogUpdater = Mockito.mock(AdvertisementLogUpdater::class.java)
        val request = Request(Method.POST, "/data/clean")
        //when
        val result = CleanupResource(advertisementLogUpdater).routes.invoke(request)
        //then
        Mockito.verify(advertisementLogUpdater).removeAllRecords()
        Assertions.assertThat(result.status).isEqualTo(Status.OK)
    }

}