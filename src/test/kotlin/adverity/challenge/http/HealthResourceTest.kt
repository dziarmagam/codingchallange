package adverity.challenge.http

import org.assertj.core.api.Assertions
import org.http4k.core.Method
import org.http4k.core.Request
import org.http4k.core.Status
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*
import org.skyscreamer.jsonassert.JSONAssert

internal class HealthResourceTest {

    private object badHealthReporter : HealthReporter {
        override fun getHealthReport() = HealthReport(false, "Bad", null, null)
    }

    private object goodHealthReporter : HealthReporter {
        override fun getHealthReport() = HealthReport(true, "Good", null, null)
    }


    @Test
    fun `should return 200 with healthy status if all dependencies are healthy`() {
        //given
        val healthReporters = listOf(goodHealthReporter, goodHealthReporter)
        val request = Request(Method.GET, "/health")
        HealthResource(healthReporters).use { healthResource ->
            healthResource.updateHealth()
            //when
            val response = healthResource.routes(request)
            //then
            Assertions.assertThat(response.status).isEqualTo(Status.OK)
            JSONAssert.assertEquals(
                """{"isHealthy":true,"name":"Service health check","information":"Service is running",
                | "dependencies":[{"isHealthy":true,"name":"Good"},{"isHealthy":true,"name":"Good"}]}""".trimMargin(), response.bodyString(), true
            )
        }

    }

    @Test
    fun `should return 500 with unhealthy status if any of dependencies are unhealthy`() {
        //given
        val healthReporters = listOf(badHealthReporter, goodHealthReporter)
        val request = Request(Method.GET, "/health")
        HealthResource(healthReporters).use { healthResource ->
            healthResource.updateHealth()
            //when
            val response = healthResource.routes(request)
            //then
            Assertions.assertThat(response.status).isEqualTo(Status.INTERNAL_SERVER_ERROR)
            JSONAssert.assertEquals(
                """{"isHealthy":false,"name":"Service health check","information":"Service is running",
                | "dependencies":[{"isHealthy":false,"name":"Bad"},{"isHealthy":true,"name":"Good"}]}""".trimMargin(), response.bodyString(), true
            )
        }

    }


}

