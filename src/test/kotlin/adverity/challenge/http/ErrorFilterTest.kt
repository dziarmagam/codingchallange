package adverity.challenge.http

import org.assertj.core.api.Assertions
import org.http4k.core.Method
import org.http4k.core.Request
import org.http4k.core.Response
import org.http4k.core.Status
import org.junit.jupiter.api.Test
import org.skyscreamer.jsonassert.JSONAssert

internal class ErrorFilterTest {

    @Test
    fun `should call source handler and return its response if no exception was thrown`(){
        //given
        val response = Response(Status.OK)
        val request = Request(Method.GET, "")
        val sourceHandler: (Request) -> Response = { response }
        //when
        val result = ErrorFilter().invoke(sourceHandler).invoke(request)
        //then
        Assertions.assertThat(result).isEqualTo(response)
    }

    @Test
    fun `given source handler thrown exception filter should return 500 response with error message`(){
        //given
        val response = Response(Status.OK)
        val request = Request(Method.GET, "")
        val sourceHandler: (Request) -> Response = { throw Exception("Error") }
        //when
        val result = ErrorFilter().invoke(sourceHandler).invoke(request)
        //then
        Assertions.assertThat(result.status).isEqualTo(Status.INTERNAL_SERVER_ERROR)
        JSONAssert.assertEquals("""{"exceptionMessage":"Error"}""",result.bodyString(), true)
    }
}