package adverity.challenge.http

import org.assertj.core.api.Assertions
import org.http4k.core.Method
import org.http4k.core.Request
import org.http4k.core.Status
import org.junit.jupiter.api.Test
import org.skyscreamer.jsonassert.JSONAssert

internal class VersionResourceTest {

    @Test
    fun `should return test version`(){
        //given
        val testVersionResourcePath = "/test-version.json"
        val request = Request(Method.GET, "/version")
        //when
        val result = VersionResource(testVersionResourcePath).routes.invoke(request)
        //then
        Assertions.assertThat(result.status).isEqualTo(Status.OK)
        JSONAssert.assertEquals("""{"version":"1.0.0", "gitHash":"githash"}""",result.bodyString(),true)
    }
}