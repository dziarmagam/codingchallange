package adverity.challenge.data

import adverity.challenge.api.*
import org.assertj.core.api.Assertions
import org.jooq.SQLDialect
import org.jooq.impl.DSL
import org.junit.jupiter.api.Test
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource

internal class JooqSqlQueryCreatorTest {

    companion object {

        @JvmStatic
        fun aggregatorOnlyTestCases(): List<Arguments> {
            return listOf(
                Arguments.of(Metric.ImpressionsSum, "sum(impressions)"),
                Arguments.of(Metric.ClicksAveragePerDay, "avg(clicks)"),
                Arguments.of(Metric.ImpressionsAveragePerDay, "avg(impressions)"),
                Arguments.of(Metric.ImpressionsSum, "sum(impressions)"),
            )
        }

        @JvmStatic
        fun filterTestCases(): List<Arguments> {
            return listOf(
                Arguments.of(
                    setOf(DatasourceFilter("Apple"), DatasourceFilter("Google")),
                    "(lower(datasource) = lower(?) OR lower(datasource) = lower(?))",
                    listOf("Apple", "Google")
                ),
                Arguments.of(
                    setOf(CampaignFilter("Campaign1"), CampaignFilter("Campaign2")),
                    "(lower(campaign) = lower(?) OR lower(campaign) = lower(?))",
                    listOf("Campaign1", "Campaign2")
                ),
                Arguments.of(
                    setOf(CampaignFilter("Campaign1"), DatasourceFilter("Google")),
                    "(lower(datasource) = lower(?) AND lower(campaign) = lower(?))",
                    listOf("Campaign1", "Google")
                ),
                Arguments.of(
                    setOf(CampaignFilter("Campaign1"), CampaignFilter("Campaign2"), DatasourceFilter("Google")),
                    "(lower(datasource) = lower(?) AND (lower(campaign) = lower(?) OR lower(campaign) = lower(?)))",
                    listOf("Campaign1", "Campaign2", "Google")
                )
            )
        }

        @JvmStatic
        fun groupByTestCases(): List<Arguments> {
            return listOf(
                Arguments.of(setOf(DimensionType.Campaign), "Campaign"),
                Arguments.of(setOf(DimensionType.Campaign, DimensionType.Datasource), "Campaign, DataSource")
            )
        }


    }

    @ParameterizedTest
    @MethodSource("aggregatorOnlyTestCases")
    fun `given query with only aggregator should return sql with only select on that aggregator and no binding`(
        metric: Metric,
        expectedMetricsSelectSql: String
    ) {
        //given
        val query = Query(aggregator = metric)
        //when
        val result = JooqSqlQueryCreator(DSL.using(SQLDialect.POSTGRES)).create(query)
        //then
        Assertions.assertThat(result.bindValues).isEmpty()
        Assertions.assertThat(result.sql).isEqualToIgnoringCase("select $expectedMetricsSelectSql as \"Metric\" from advertisement_log")
    }

    @Test
    fun `given query with CTR aggregator should return sql with only select on CTR and clicks != 0`() {
        //given
        val query = Query(aggregator = Metric.CTR)
        val metricsSelectSql = "(sum(impressions) / sum(clicks))"
        //when
        val result = JooqSqlQueryCreator(DSL.using(SQLDialect.POSTGRES)).create(query)
        //then
        Assertions.assertThat(result.bindValues).hasSize(1)
        Assertions.assertThat(result.bindValues[0]).isEqualTo(0L)
        Assertions.assertThat(result.sql).isEqualToIgnoringCase("select $metricsSelectSql as \"Metric\" from advertisement_log where clicks <> ?")
    }

    @ParameterizedTest
    @MethodSource("filterTestCases")
    fun `given query with filter should return sql with correct where query and bindings`(
        filters: Set<QueryFilter>,
        expectedWhereSql: String,
        expectedBinding: List<String>
    ) {
        //given
        val query = Query(aggregator = Metric.ClicksSum, filters = filters)
        //when
        val result = JooqSqlQueryCreator(DSL.using(SQLDialect.POSTGRES)).create(query)
        //then
        Assertions.assertThat(result.bindValues).containsOnlyElementsOf(expectedBinding)
        Assertions.assertThat(result.sql).isEqualToIgnoringCase("select sum(clicks) as \"Metric\" from advertisement_log where $expectedWhereSql")
    }


    @ParameterizedTest
    @MethodSource("groupByTestCases")
    fun `given query with groupBy should return sql with correct group by and select`(
        groupBy: Set<DimensionType>,
        expectedGroupByAndSelectSql: String
    ) {
        //given
        val query = Query(aggregator = Metric.ClicksSum, groupBy = groupBy)
        //when
        val result = JooqSqlQueryCreator(DSL.using(SQLDialect.POSTGRES)).create(query)
        //then
        Assertions.assertThat(result.bindValues).isEmpty()
        Assertions.assertThat(result.sql)
            .isEqualToIgnoringCase("select $expectedGroupByAndSelectSql, sum(clicks) as \"Metric\" from advertisement_log group by $expectedGroupByAndSelectSql")
    }

    @Test
    fun `given query with filters and group by should return correct sql`() {
        //given
        val query = Query(setOf(CampaignFilter("AppleCampaign")), setOf(DimensionType.Datasource), Metric.ClicksSum)
        //when
        val result = JooqSqlQueryCreator(DSL.using(SQLDialect.POSTGRES)).create(query)
        //then
        Assertions.assertThat(result.bindValues).containsOnly("AppleCampaign")
        Assertions.assertThat(result.sql)
            .isEqualToIgnoringCase("select datasource, sum(clicks) as \"Metric\" from advertisement_log where lower(campaign) = lower(?) group by datasource")

    }

}