package adverity.challenge.data

import adverity.challenge.api.Metric
import adverity.challenge.api.Query
import adverity.challenge.api.QueryResult
import org.assertj.core.api.Assertions
import org.jooq.Record
import org.jooq.RecordMapper
import org.jooq.Result
import org.jooq.SelectHavingStep
import org.junit.jupiter.api.Test
import org.mockito.Mockito
import java.math.BigDecimal
import java.sql.Connection
import java.sql.Date
import java.sql.PreparedStatement
import java.time.LocalDate
import javax.sql.DataSource

internal class PostgresAdvertisementLogDaoTest {

    private val datasource = Mockito.mock(DataSource::class.java)
    private val queryCreator = Mockito.mock(JooqSqlQueryCreator::class.java)

    @Test
    fun `when inserting records into data should set prepared statement params`() {
        //given
        val connection = Mockito.mock(Connection::class.java)
        val preparedStatement = Mockito.mock(PreparedStatement::class.java)
        Mockito.`when`(datasource.connection).thenReturn(connection)
        Mockito.`when`(connection.prepareStatement(Mockito.any())).thenReturn(preparedStatement)
        val advertisementLog1 = AdvertisementLog("datasource1", "campaign1", LocalDate.of(2020, 1, 1), 1L, 1L)
        val advertisementLog2 = AdvertisementLog("datasource2", "campaign2", LocalDate.of(2020, 2, 2), 2L, 2L)
        val records = listOf(advertisementLog1, advertisementLog2)
        //when
        PostgresAdvertisementLogDao(datasource, queryCreator).addRecords(records)
        //then
        verifyPreparedStatementSets(preparedStatement, advertisementLog1)
        verifyPreparedStatementSets(preparedStatement, advertisementLog2)
    }

    @Test
    fun `given query result should map return records to Query object`() {
        //given
        val query = Query(aggregator = Metric.CTR)
        val preparedQuery: SelectHavingStep<Record> = createMock()
        val record = createMockRecord(BigDecimal.ONE, "Campaign", "Datasource")
        val recordCaptor = createRecordCapture()
        Mockito.`when`(queryCreator.create(query)).thenReturn(preparedQuery)
        Mockito.`when`(preparedQuery.fetch()).thenReturn(recordCaptor)
        Mockito.`when`(recordCaptor.isNotEmpty).thenReturn(true)
        //when
        PostgresAdvertisementLogDao(datasource, queryCreator).query(query)
        val mapperResult = recordCaptor.mapper?.map(record)
        //then
        Assertions.assertThat(mapperResult).isInstanceOf(QueryResult::class.java)
        Assertions.assertThat(mapperResult).isNotNull()
        val queryResult = mapperResult as QueryResult
        Assertions.assertThat(queryResult.campaign).isEqualTo("Campaign")
        Assertions.assertThat(queryResult.datasource).isEqualTo("Datasource")
        Assertions.assertThat(queryResult.aggregator).isEqualTo(Metric.CTR)
        Assertions.assertThat(queryResult.value).isEqualTo(1.0)
    }


    @Test
    fun `given query result with campaign or datasource should map return records to Query object without campaign or datasource`() {
        //given
        val query = Query(aggregator = Metric.CTR)
        val preparedQuery: SelectHavingStep<Record> = createMock()
        val record = createMockRecord(BigDecimal.ONE, null, null)
        val recordCaptor = createRecordCapture()
        Mockito.`when`(queryCreator.create(query)).thenReturn(preparedQuery)
        Mockito.`when`(preparedQuery.fetch()).thenReturn(recordCaptor)
        Mockito.`when`(recordCaptor.isNotEmpty).thenReturn(true)
        //when
        PostgresAdvertisementLogDao(datasource, queryCreator).query(query)
        val mapperResult = recordCaptor.mapper?.map(record)
        //then
        Assertions.assertThat(mapperResult).isInstanceOf(QueryResult::class.java)
        Assertions.assertThat(mapperResult).isNotNull()
        val queryResult = mapperResult as QueryResult
        Assertions.assertThat(queryResult.campaign).isNull()
        Assertions.assertThat(queryResult.datasource).isNull()
        Assertions.assertThat(queryResult.aggregator).isEqualTo(Metric.CTR)
        Assertions.assertThat(queryResult.value).isEqualTo(1.0)
    }

    private fun verifyPreparedStatementSets(preparedStatement: PreparedStatement, advertisementLog: AdvertisementLog) {
        Mockito.verify(preparedStatement).setString(1, advertisementLog.datasource)
        Mockito.verify(preparedStatement).setString(2, advertisementLog.campaign)
        Mockito.verify(preparedStatement).setDate(3, Date.valueOf(advertisementLog.daily))
        Mockito.verify(preparedStatement).setLong(4, advertisementLog.clicks)
        Mockito.verify(preparedStatement).setLong(5, advertisementLog.impressions)
    }

    private fun createMockRecord(metric: BigDecimal, campaign: String?, datasource: String?): Record {
        val record = Mockito.mock(Record::class.java)
        Mockito.`when`(record["Metric"]).thenReturn(metric)
        Mockito.`when`(record["Campaign"]).apply {
            if (campaign == null) thenThrow(IllegalArgumentException()) else thenReturn(campaign)
        }
        Mockito.`when`(record["Datasource"]).apply {
            if (datasource == null) thenThrow(IllegalArgumentException()) else thenReturn(datasource)
        }
        return record
    }

    private inline fun <reified T> createMock(): T {
        return Mockito.mock(T::class.java)
    }

    private fun createRecordCapture() = object : Result<Record> by createMock() {
        var mapper: RecordMapper<in Record, *>? = null

        override fun <E> map(mapper: RecordMapper<in Record, E>?): MutableList<E> {
            this.mapper = mapper
            return mutableListOf()
        }
    }
}