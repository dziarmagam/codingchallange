package adverity.challenge.data

import org.jooq.SQLDialect
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.ValueSource
import org.mockito.Mockito
import java.sql.Connection
import java.sql.DatabaseMetaData
import javax.sql.DataSource

internal class DatabaseUpdaterTest {

    @Test
    fun `given not supported database should skip update`() {
        //given
        val dataSource = Mockito.mock(DataSource::class.java)
        val sqlDialect = SQLDialect.DERBY
        //when
        DatabaseUpdater(dataSource, sqlDialect).update()
        //then
        Mockito.verify(dataSource, Mockito.times(0)).connection
    }
}