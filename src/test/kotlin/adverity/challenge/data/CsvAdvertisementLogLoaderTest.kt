package adverity.challenge.data

import adverity.challenge.data.CsvAdvertisementLogLoader.*
import org.assertj.core.api.Assertions
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.fail
import org.mockito.Mockito
import java.time.LocalDate

internal class CsvAdvertisementLogLoaderTest {

    private val missingHeaderCSVContent =
        """Datasource,Campaign,Daily,Clicks
Google Ads,Adventmarkt Touristik,11/12/19,7,22425
Google Ads,Adventmarkt Touristik,11/13/19,16,45452"""
    private val wrongHeaderNameCSVContent =
        """Datasource,Campaign,Daily,Clicks_wrong,Impressions
Google Ads,Adventmarkt Touristik,11/12/19,7,22425
Google Ads,Adventmarkt Touristik,11/13/19,16,45452"""
    private val wrongDateFormatCSVContent =
        """Datasource,Campaign,Daily,Clicks,Impressions
Google Ads,Adventmarkt Touristik,11-12/19,7,22425
Google Ads,Adventmarkt Touristik,11/13/19,16,45452"""
    private val wrongClicksFormatCSVContent =
        """Datasource,Campaign,Daily,Clicks,Impressions
Google Ads,Adventmarkt Touristik,11/12/19,7,45452
Google Ads,Adventmarkt Touristik,11/13/19,wrong,16"""
    private val validCSVContent =
        """Datasource,Campaign,Daily,Clicks,Impressions
Google Ads,Adventmarkt Touristik,11/12/19,7,22425
Google Ads,Adventmarkt Touristik,11/13/19,16,45452"""

    private val advertisementLogUpdater = Mockito.mock(AdvertisementLogUpdater::class.java)

    @Test
    fun `should throw exception when csv content has missing header`() {
        //given
        val csvWarehouseDataLoader = CsvAdvertisementLogLoader(advertisementLogUpdater)
        //when
        try {
            csvWarehouseDataLoader.load(missingHeaderCSVContent.byteInputStream())
            fail("Exception was not thrown")
        } catch (e: InvalidCsvHeaders) {
            //then
            Assertions.assertThat(e.givenHeaders).isEqualTo(listOf("Datasource", "Campaign", "Daily", "Clicks"))
        }
    }

    @Test
    fun `should throw exception when csv content has wrong header name`() {
        //given
        val csvWarehouseDataLoader = CsvAdvertisementLogLoader(advertisementLogUpdater)
        //when
        try {
            csvWarehouseDataLoader.load(wrongHeaderNameCSVContent.byteInputStream())
            fail("Exception was not thrown")
        } catch (e: InvalidCsvHeaders) {
            //then
            Assertions.assertThat(e.givenHeaders).isEqualTo(listOf("Datasource", "Campaign", "Daily", "Clicks_wrong", "Impressions"))
        }
    }

    @Test
    fun `should throw exception when csv content has wrong date format`() {
        //given
        val csvWarehouseDataLoader = CsvAdvertisementLogLoader(advertisementLogUpdater)
        //when
        try {
            csvWarehouseDataLoader.load(wrongDateFormatCSVContent.byteInputStream())
            fail("Exception was not thrown")
        } catch (e: CsvParsingFailed) {
            //then
            Assertions.assertThat(e.originalMessage).isEqualTo("Text '11-12/19' could not be parsed at index 2")
            Assertions.assertThat(e.recordNumber).isEqualTo(2)
        }
    }

    @Test
    fun `should throw exception when csv content has clicks date format`() {
        //given
        val csvWarehouseDataLoader = CsvAdvertisementLogLoader(advertisementLogUpdater)
        //when
        try {
            csvWarehouseDataLoader.load(wrongClicksFormatCSVContent.byteInputStream())
            fail("Exception was not thrown")
        } catch (e: CsvParsingFailed) {
            //then
            Assertions.assertThat(e.recordNumber).isEqualTo(3)
            Assertions.assertThat(e.message).isEqualTo("""Failed parsing record 3, message: For input string: "wrong"""")
        }
    }

    @Test
    fun `should parse valid csv content`() {
        //given
        val csvWarehouseDataLoader = CsvAdvertisementLogLoader(advertisementLogUpdater)
        //when
        csvWarehouseDataLoader.load(validCSVContent.byteInputStream())
        //then
        Mockito.verify(advertisementLogUpdater, Mockito.times(1))
            .addRecords(
                listOf(
                    AdvertisementLog("Google Ads", "Adventmarkt Touristik", LocalDate.of(2019, 11, 12), 7, 22425),
                    AdvertisementLog("Google Ads", "Adventmarkt Touristik", LocalDate.of(2019, 11, 13), 16, 45452)
                )
            )

    }
}