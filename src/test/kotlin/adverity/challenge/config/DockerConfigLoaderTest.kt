package adverity.challenge.config

import org.assertj.core.api.Assertions
import org.jooq.SQLDialect
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.fail
import org.junitpioneer.jupiter.SetEnvironmentVariable

internal class DockerConfigLoaderTest {


    @Test
    @SetEnvironmentVariable.SetEnvironmentVariables(
        SetEnvironmentVariable(key = DockerConfigLoader.HTTP_PORT, value = "9000"),
        SetEnvironmentVariable(key = DockerConfigLoader.DATABASE_URL, value = "url"),
        SetEnvironmentVariable(key = DockerConfigLoader.DATABASE_DIALECT, value = "H2"),
        SetEnvironmentVariable(key = DockerConfigLoader.DATABASE_DRIVER, value = "driver"),
        SetEnvironmentVariable(key = DockerConfigLoader.DATABASE_USER, value = "user"),
        SetEnvironmentVariable(key = DockerConfigLoader.DATABASE_PASSWORD, value = "password"),
    )
    fun `should create config out of system environment variables `() {
        //given
        //environment variables
        //when
        val result = DockerConfigLoader().load()
        //then
        Assertions.assertThat(result.port).isEqualTo(9000)
        Assertions.assertThat(result.databaseConfig.driverClassName).isEqualTo("driver")
        Assertions.assertThat(result.databaseConfig.url).isEqualTo("url")
        Assertions.assertThat(result.databaseConfig.password).isEqualTo("password")
        Assertions.assertThat(result.databaseConfig.username).isEqualTo("user")
        Assertions.assertThat(result.databaseConfig.sqlDialect).isEqualTo(SQLDialect.H2)
    }

    @Test
    @SetEnvironmentVariable.SetEnvironmentVariables(
        SetEnvironmentVariable(key = DockerConfigLoader.DATABASE_URL, value = "url"),
        SetEnvironmentVariable(key = DockerConfigLoader.DATABASE_DIALECT, value = "H2"),
        SetEnvironmentVariable(key = DockerConfigLoader.DATABASE_DRIVER, value = "driver"),
        SetEnvironmentVariable(key = DockerConfigLoader.DATABASE_USER, value = "user"),
        SetEnvironmentVariable(key = DockerConfigLoader.DATABASE_PASSWORD, value = "password"),
    )
    fun `should throw MissingEnvironmentProperty exception when required property is missing`() {
        //given
        //no environment variables
        //when
        try {
            val result = DockerConfigLoader().load()
            fail("No exception was thrown")
        } catch (e: MissingEnvironmentProperty) {
            //then
            Assertions.assertThat(e.propertyName).isEqualTo(DockerConfigLoader.HTTP_PORT)
        }

    }
}