./gradlew assemble
docker build -t adverity-challenge .
docker tag adverity-challenge:latest 181286920649.dkr.ecr.eu-central-1.amazonaws.com/adverity-challenge:latest
docker push 181286920649.dkr.ecr.eu-central-1.amazonaws.com/adverity-challenge:latest
